const { assert } = require('chai'),
  redis = require('redis'),
  sinon = require('sinon'),
  redisMock = require('../mocks/redis')

// eslint-disable-next-line no-undef
describe('redis', () => {
  let redisService = {}
  // eslint-disable-next-line no-undef
  before(function(){
    sinon.stub(redis, 'createClient').returns(redisMock)
    redisService = require('../data/redis')
  })
  // eslint-disable-next-line no-undef
  describe('pushList', () => {
    // eslint-disable-next-line no-undef
    it('Push users in list', async () => {
      const key = 'lusers',
        value = 'sample-value',
        result = await redisService.pushList(key, value)

      assert.isNumber(result)
    })
    // eslint-disable-next-line no-undef
    it('Get users list', async () => {
      const key = 'lusers',
        result = await redisService.getList(key)
      assert.isArray(result)
    })
    // eslint-disable-next-line no-undef
    it('Set item', async () => {
      const key = 'sample:key',
        value = 'sample-value',
        result = await redisService.setItem(key, value, 10)
      assert.equal(result, 'OK')
    })
    // eslint-disable-next-line no-undef
    it('Find item', async () => {
      const key = '*key*',
        result = await redisService.find(key)
      assert.equal(result[0],  'sample:key' )
    })
    // eslint-disable-next-line no-undef
    it('Find user', async () => {
      const key = 'lusers',
        username = 'sample-value',
        result = await redisService.findUser(key, username)
      assert.isString(result)
    })
  })
})

