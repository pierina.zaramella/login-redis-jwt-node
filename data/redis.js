const redis = require('redis')
const client = redis.createClient('redis://127.0.0.1:6379')

const pushList = (item, key) => {
  return new Promise((resolve, reject) => {
    //prevent duplicates
    client.RPUSH(key, item, (err, val) => {
      if(err)
        reject(err)
      else
        resolve(val)
    })
  })
}

const getList = (key) => {
  return new Promise((resolve, reject) => {
    client.lrange(key, 0, -1, (err, val) => {
      if(err)
        reject(err)
      else
        resolve(val)
    })
  })
}

const setItem = (key, item, tExpSec) => {
  return new Promise((resolve, reject) => {
    client.SETEX(key, tExpSec, item, (err, val) => {
      if(err)
        reject(err)
      else
        resolve(val)
    })
  })
}

const find = (criterie)=> {
  return new Promise((resolve, reject) => {
    client.KEYS(criterie, (err, items) => {
      if(err)
        reject(err)
      else
        resolve(items)
    })
  })
}

const findUser = async(key, username) => {
  const users = await getList(key)
  const exists = users.find(user => JSON.parse(user).username === username)
  return exists
}

const deleteUser = (listKey) => {
  return client.DEL(listKey)
}

const deleteAll = (listKey) =>{
  client.ltrim(listKey, -1, 0) 
}

module.exports = { pushList, getList, setItem, find, findUser, deleteUser, deleteAll }