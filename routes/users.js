const express = require('express')
const router = express.Router()
const Users = require('../controllers/users')
const usersController = new Users()
console.log(usersController)

/* Create user. */
router.post('/register', usersController.register)

/* Autenticate user. */
router.post('/login', usersController.login)

/* Autenticate list users. */
router.post('/authenticated', usersController.authenticated)

/* Logout user. */
router.post('/logout', usersController.logout)

module.exports = router
