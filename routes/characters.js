const express = require('express')
const router = express.Router()
const Characters = require('../controllers/characters')
const charactersController = new Characters()

/* GET home page. */
router.get('/', charactersController.characters)

module.exports = router