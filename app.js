require('dotenv').config();
const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const jwt = require('./config/jwt');
const errorHandler = require('./config/errorHandler');
const cors = require('cors')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const charactersRouter = require('./routes/characters')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(jwt())
app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/characters', charactersRouter)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(errorHandler)

module.exports = app
