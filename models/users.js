const Joi = require('joi')

const schemaRegister = Joi.object({
  name: Joi.string().required(),
  lastname: Joi.string().required(),
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),

  password: Joi.string().required()
})

const schema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  password: Joi.string().required()
})

const validateUserRegister = async (user) => {
  return await schemaRegister.validate(user)
}

const validateUserLogin = async (user) => {
  return await schema.validate(user)
}

module.exports = { validateUserRegister, validateUserLogin }
