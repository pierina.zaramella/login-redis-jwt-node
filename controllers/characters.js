const axios = require('axios')

class CharactersController {
  async characters(req, res, next){
    try{
      const { data: { results } } = await axios.get(process.env.apiEndpoint)

      console.log(results)

      const filtered= results.map(item => {
        const { name, status, species, gender, image } = item
        return { name, status, species, gender, image }
      })

      res.send(filtered)
    } catch(e){
      next(e)
    }
  }
}

module.exports = CharactersController
