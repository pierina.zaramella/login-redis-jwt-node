const { pushList, getList, setItem, find, findUser, deleteUser } = require('../data/redis')
const { validateUserLogin, validateUserRegister } = require('../models/users')
const jwt = require('jsonwebtoken')

class UsersController {
   /**
   * Register a user
   * @constructor
   * @param {JSON} req.body - It Has the information of new user.
   *      @param {string} name - The name of the user.
   *      @param {string} lastname - The lastname of the user.
   *      @param {string} username - The username of the user.
   *      @param {string} password - The password of the user.
   * @example body -> raw (JSON): { "name": "Rick", "lastname": "Sanchez", "username": "abu", "password": "ricksanchez" }
  */
  async register(req, res, next) {
    let stUser = '', valid = '', username = ''
    try {
      stUser = JSON.stringify(req.body)
      valid = await validateUserRegister(req.body)

      if (JSON.stringify(valid) === stUser) {
        username = req.body.username
        const registeredUser = await findUser('lusers', username)
        if (registeredUser) {
          res.statusCode = 409
          res.send('Usuario ya existe.')
        } else {
            await pushList(stUser, 'lusers')
            res.statusCode = 201
            res.send('Usuario registrado exitosamente')
        }
      } else {
        res.statusCode = 409
        res.send('Datos errados.')
      }
    } catch (e) {
      next(e)
    }
  }
  /**
   * Login a user
   * @constructor
   * @param {JSON} req.body - It Has the information of login existing user.
   *      @param {string} username - The username of the user.
   *      @param {string} password - The password of the user.
   * @example body -> raw (JSON): { "username": "abu", "password": "ricksanchez" }
  */
  async login(req, res, next){
    try{
      const valid = await validateUserLogin(req.body)
      const stUser = JSON.stringify(req.body)

      if(!valid || JSON.stringify(valid) === stUser){
        const { username, password } = req.body
        const users = await getList('lusers')
        console.log(users)
        const user = users.find(user => {
          const u = JSON.parse(user)
          return u.username === username && u.password === password
        })

        if(user){
          const exp = +process.env.expirationTime
          const token = jwt.sign({ sub: username }, process.env.secret, {
            algorithm: 'HS256',
            expiresIn: exp
          })

          await setItem('authenticated:'+username, user, exp)
          res.send({ message: 'Usuario autenticado exitosamente', token })
        } else {
          res.status(404).send('Usuario no existe.')
        }
      } else {
        res.status(409).send('Ocurrio un error al autenticar al usuario.')
      }
    } catch(e) {
      next(e)
    }
  }
  /**
   * Get authenticated users list  
   * @constructor
   * @param {string} Token: From /login put the Authorization Bearer token..
   * @example Header -> Authorization: Bearer <token from login>
  */
  async authenticated(req, res, next){
    try{
      let activeUsers = await find('*authenticated*')
      const users = await getList('lusers')
      activeUsers = activeUsers.map(i => {
        const replUsername = i.replace('authenticated:', '')
        const us = users.find(user => {
          const u = JSON.parse(user)
          return u.username === replUsername
        })
        const { username, name, lastname } = JSON.parse(us)
        return { username, name, lastname }
      })
      res.send(activeUsers)
    } catch(e){
      next(e)
    }
  }

  /**
   * Logout a user
   * @constructor
   * @param {JSON} body - It Has the information of login existing user.
   *      @param {string} username - The username of the user.
  */
  async logout(req, res, next){
    try{
      const { username } = req.body
      let activeUser = await find(`authenticated:${username}`)
      console.log('activeUser', activeUser)

      if(activeUser.length > 0){
        deleteUser(activeUser[0], activeUser)
        //TODO: check response
      }
      res.send('Cerrar sesión completado correctamente.')
    } catch(e){
      next(e)
    }
  }
}

module.exports = UsersController
