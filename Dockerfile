#FROM redis
#COPY redis.conf /usr/local/etc/redis/redis.conf
#CMD [ "redis-server", "/usr/local/etc/redis/redis.conf" ]


FROM node:latest as node-api
### copy modules
COPY package.json .
COPY bin bin
COPY config config
COPY models models
COPY public public
COPY data data
COPY routes routes
COPY views views
COPY app.js .

### install dependencies
RUN npm install modclean -g
RUN npm install --silent
RUN modclean -r

# Please, check & delete unused files
FROM node:alpine
RUN mkdir /var/www
WORKDIR /var/www
COPY --from=node-api bin bin
COPY --from=node-api config config
COPY --from=node-api models models
COPY --from=node-api public public
COPY --from=node-api routes routes
COPY --from=node-api views views
#COPY __tests__ __tests__
COPY --from=node-api app.js .
COPY --from=node-api node_modules node_modules
COPY --from=node-api package.json package.json

###set enviroments vars
ENV secret="djghhhhuuwiwuewieuwieuriwu"
ENV apiEndpoint = "https://rickandmortyapi.com/api/character"
ENV expirationTime = 120

###start app
EXPOSE 3000
CMD node ./bin/www.js