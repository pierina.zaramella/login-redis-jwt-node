const list = []
const set = {}
const redisMock = {
  RPUSH: (key, value, callback) => {
    if(key && value){
      list.push(JSON.stringify({ username: key, password: value }))
      callback('', list.length)
    } else {
      callback('missing params', 0)
    }
  },
  lrange: (key, init, end, callback) => {
    if(key){
      callback('', list)
    } else {
      callback('missing params', 0)
    }
  },
  SETEX: (key, tExpSec, value, callback) => {
    if(key && value && typeof(+(tExpSec)) === 'number'){
      set[key] = value
      callback('', 'OK')
      setTimeout((key) => {
        delete set[key]
      }, tExpSec)
    } else {
      callback('missing params', 0)
    }
  },
  KEYS: (param, callback) => {
    if(param){
      const keys = Object.keys(set)
      param = param.replace(RegExp(/\*/g), '')
      const result = keys.reduce((acc, item) => {
        if(item.indexOf(param) > -1)
          acc.push(item)
        return acc
      }, [])
      callback('',result)
    } else {
      callback('missing params', 0)
    }
  },
  DEL: (key, callback) => {
    if(key){
      const index = list.findIndex(x => x.key === key)

      if(index > -1){
        list.splice(index, 1)
        callback('',1)
      }
    } else {
      callback('missing params', 0)
    }
  }
}

module.exports = redisMock